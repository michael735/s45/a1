import {Card, Button, Container, Col, Row} from 'react-bootstrap';
import { LoremIpsum, Avatar } from 'react-lorem-ipsum';

export default function CourseCard() {

    return(
        <Container className='mb-5 pb-5'>
            <Row className="d-flex">

                {/* Card 1 */}
                
                <Col classname="m-0 p-0">
                    <Card >
                        <Card.Body>
                            <Card.Title>
                                <h3 className='display-5 font-weight-bold'>Learn from Home</h3>
                            </Card.Title>
                            <Card.Text>
                                <LoremIpsum p={1} />
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                {/* Card 2 */}

                <Col classname="m-0 p-0">
                    <Card>
                        <Card.Body>
                            <Card.Title>
                                <h3 className='display-5 font-weight-bold'>Study Now, Pay later</h3>
                            </Card.Title>
                            <Card.Text>
                                 <LoremIpsum p={1} />
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                {/* Card 3 */}

                <Col classname="m-0 p-0">
                    <Card>
                        <Card.Body>
                            <Card.Title>
                                <h3 className='display-5 font-weight-bold'>Be part of Our Community</h3>
                            </Card.Title>
                            <Card.Text>
                                <LoremIpsum p={1} />
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                {/* Card 4 */}
                <Col className='mb-5'>
                    <Card>
                        <Card.Body>
                            <Card.Title className='font-weight-bold'>Sample Course</Card.Title>
                            <Card.Text>
                                <p className="m-0">Description:</p>
                                <p>This is a sample course offering</p>
                                <p className="m-0">Price:</p>
                                <p>PhP 40,000</p>
                            </Card.Text>
                            <Button variant="primary">Enroll</Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>     
        </Container>
    )
}